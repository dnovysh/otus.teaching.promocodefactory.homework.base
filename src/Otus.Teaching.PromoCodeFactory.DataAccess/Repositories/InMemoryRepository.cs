﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T, TRestricted>
        : IRepository<T, TRestricted>
        where T : BaseEntity, ICanBeCreatedFromRestricted<TRestricted>, new()
        where TRestricted : class
    {
        private ConcurrentDictionary<Guid, T> _data;

        private T Add(TRestricted restrictedEntity)
        {
            Guid id;
            T entity;

            do
            {
                id = Guid.NewGuid();
                entity = new T();
                entity.CreateFromRestricted(id, restrictedEntity);
            } while (!_data.TryAdd(id, entity));

            return entity;
        }

        private bool Replace(T entity)
        {
            if (_data.ContainsKey(entity.Id))
            {
                _data[entity.Id] = entity;
                return true;
            }

            return false;
        }

        public InMemoryRepository() : this(null)
        {
        }

        public InMemoryRepository(IEnumerable<T> data)
        {
            if (data != null && data.Any())
            {
                _data = new ConcurrentDictionary<Guid, T>(
                    data.Select(x => new KeyValuePair<Guid, T>(x.Id, x)));
            }
            else
            {
                _data = new ConcurrentDictionary<Guid, T>();
            }
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(_data.Values.AsEnumerable());
        }

        public Task<List<T>> GetAllOrNothingByIDsAsync(IEnumerable<Guid> ids)
        {
            var entities = new List<T>();

            foreach (var id in ids)
            {
                if (!_data.TryGetValue(id, out T entity))
                {
                    return Task.FromResult((List<T>)null);
                }

                entities.Add(entity);
            }

            return Task.FromResult(entities);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            if (_data.TryGetValue(id, out T res))
            {
                return Task.FromResult(res);
            }

            return Task.FromResult((T)null);
        }

        public Task<T> AddAsync(TRestricted restrictedEntity)
        {
            return Task.FromResult(Add(restrictedEntity));
        }

        public Task<bool> ReplaceAsync(T entity)
        {
            return Task.FromResult(Replace(entity));
        }

        public Task<bool> RemoveByIdAsync(Guid id)
        {
            return Task.FromResult(_data.TryRemove(id, out _));
        }
    }
}
