﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class EmployeeRestricted
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<Role> Roles { get; set; }
        public int AppliedPromocodesCount { get; set; }

        public EmployeeRestricted(string firstName,
                                  string lastName,
                                  string email,
                                  List<Role> roles,
                                  int appliedPromocodesCount = 0)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Roles = roles;
            AppliedPromocodesCount = appliedPromocodesCount;
        }
    }
}
