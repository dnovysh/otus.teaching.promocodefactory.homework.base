﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class RoleRestricted
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public RoleRestricted(string name, string description)
        {
            Name = name;
            Description = description;
        }
    }
}