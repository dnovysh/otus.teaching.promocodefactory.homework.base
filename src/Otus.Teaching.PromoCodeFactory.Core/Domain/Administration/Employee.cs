﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee : BaseEntity, ICanBeCreatedFromRestricted<EmployeeRestricted>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<Role> Roles { get; set; }
        public int AppliedPromocodesCount { get; set; }
        public string FullName => $"{FirstName} {LastName}";

        public Employee() { }

        public Employee(Guid id,
                        string firstName,
                        string lastName,
                        string email,
                        List<Role> roles,
                        int appliedPromocodesCount) 
            : base(id)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Roles = roles;
            AppliedPromocodesCount = appliedPromocodesCount;
        }

        public void CreateFromRestricted(Guid id, EmployeeRestricted employeeRestricted)
        {
            Id = id;
            FirstName = employeeRestricted.FirstName;
            LastName = employeeRestricted.LastName;
            Email = employeeRestricted.Email;
            Roles = employeeRestricted.Roles;
            AppliedPromocodesCount = employeeRestricted.AppliedPromocodesCount;
        }
    }
}