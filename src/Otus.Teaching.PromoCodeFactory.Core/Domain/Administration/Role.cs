﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Role : BaseEntity, ICanBeCreatedFromRestricted<RoleRestricted>
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public void CreateFromRestricted(Guid id, RoleRestricted roleRestricted)
        {
            Id = id;
            Name = roleRestricted.Name;
            Description = roleRestricted.Description;
        }
    }
}