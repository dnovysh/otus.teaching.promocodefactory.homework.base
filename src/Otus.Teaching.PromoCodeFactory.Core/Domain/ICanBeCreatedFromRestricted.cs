﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
    public interface ICanBeCreatedFromRestricted <TRestricted>
        where TRestricted : class
    {
        void CreateFromRestricted(Guid id, TRestricted restrictedEntity);
    }
}
