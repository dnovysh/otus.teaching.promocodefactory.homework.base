﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
    public class BaseEntity
    {
        public Guid Id { get; set; } = Guid.Empty;

        public BaseEntity() { }

        public BaseEntity(Guid id)
        {
            Id = id;
        }
    }
}
