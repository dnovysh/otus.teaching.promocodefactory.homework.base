﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T, TRestricted>
        where T : BaseEntity
        where TRestricted : class
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<List<T>> GetAllOrNothingByIDsAsync(IEnumerable<Guid> ids);
        Task<T> GetByIdAsync(Guid id);
        Task<T> AddAsync(TRestricted restrictedEntity);
        Task<bool> ReplaceAsync(T entity);
        Task<bool> RemoveByIdAsync(Guid id);
    }
}
