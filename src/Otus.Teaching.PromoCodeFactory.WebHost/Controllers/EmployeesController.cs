﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController : ControllerBase
    {
        private readonly IRepository<Employee, EmployeeRestricted> _employeeRepository;
        private readonly IRepository<Role, RoleRestricted> _roleRepository;

        public EmployeesController(IRepository<Employee, EmployeeRestricted> employeeRepository,
            IRepository<Role, RoleRestricted> rolesRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = rolesRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            return employees.Select(e => new EmployeeShortResponse(e)).ToList();
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}", Name = "GetEmployeeByIdAsync")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee != null)
            {
                return Ok(new EmployeeResponse(employee));
            }

            return NotFound("404 Not Found");
        }

        /// <summary>
        /// Добавить нового сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<EmployeeResponse>> AddEmployeeAsync(
            [FromBody] EmployeeCreateModel employeeCreateModel)
        {
            if (employeeCreateModel == null)
            {
                return BadRequest("Employee object is null");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            var employeeRestricted = await employeeCreateModel
                .ToEmployeeRestrictedAsync(_roleRepository);

            if (employeeRestricted == null)
            {
                return Conflict("409 Role Not Found");
            }

            var employee = await _employeeRepository.AddAsync(employeeRestricted);

            var employeeResponse = new EmployeeResponse(employee);

            return CreatedAtRoute("GetEmployeeByIdAsync",
                new { id = employeeResponse.Id }, employeeResponse);
        }

        /// <summary>
        /// Заменить контент сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> PutEmployeeByIdAsync(Guid id, 
            [FromBody] EmployeeReplaceModel employeeReplaceModel)
        {
            if (employeeReplaceModel == null)
            {
                return BadRequest("Employee object is null");
            }

            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid model object");
            }

            var employee = await employeeReplaceModel.ToEmployee(id, _roleRepository);

            if (employee == null)
            {
                return Conflict("409 Role Not Found");
            }

            if (!await _employeeRepository.ReplaceAsync(employee))
            {
                return NotFound($"404 Not Found. Employee with id = {id} not found");
            }

            return NoContent();
        }

        /// <summary>
        /// Удалить сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteEmployeeByIdAsync(Guid id)
        {
            if (!await _employeeRepository.RemoveByIdAsync(id))
            {
                return NotFound($"404 Not Found. Employee with id = {id} not found");
            }

            return NoContent();
        }
    }
}
