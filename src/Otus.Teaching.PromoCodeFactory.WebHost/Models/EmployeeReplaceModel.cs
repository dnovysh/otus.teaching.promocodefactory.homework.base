﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeReplaceModel
    {
        [Required(ErrorMessage = "FirstName is required")]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<Guid> RoleIDs { get; set; }
        public int AppliedPromocodesCount { get; set; }

        public async Task<Employee> ToEmployee(Guid id,
            IRepository<Role, RoleRestricted> roleRepository)
        {
            var roleTuple = await EmployeeModelHelper
                .TryGetRolesAsyncByIDs(RoleIDs, roleRepository);

            if (roleTuple.Item1)
            {
                return new Employee(id, FirstName, LastName, Email,
                    roleTuple.Item2, AppliedPromocodesCount);
            }

            return null;
        }
    }
}
