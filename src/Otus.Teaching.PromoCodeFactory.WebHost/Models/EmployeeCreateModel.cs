﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeCreateModel
    {
        [Required(ErrorMessage = "FirstName is required")]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<Guid> RoleIDs { get; set; }

        public async Task<EmployeeRestricted> ToEmployeeRestrictedAsync(
            IRepository<Role, RoleRestricted> roleRepository)
        {
            var roleTuple = await EmployeeModelHelper
                .TryGetRolesAsyncByIDs(RoleIDs, roleRepository);

            if (roleTuple.Item1)
            {
                return new EmployeeRestricted(FirstName, LastName, Email, roleTuple.Item2);
            }

            return null;
        }
    }
}
