﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public static class EmployeeModelHelper
    {
        /// <summary>
        /// Получить список ролей по списку гуидов ролей, список гуидов может
        /// быть null или пустой, в этом случае операция успешна и возвращается
        /// null в качестве списка ролей, в противном случае по идентификаторам
        /// должны быть получены все роли, если каких то ролей нет, то операция
        /// не успешна.
        /// </summary>
        /// <param name="roleIDs">List гуидов ролей модели сотрудника</param>
        /// <param name="roleRepository">Репозиторий ролей</param>
        /// <returns></returns>
        public static async Task<Tuple<bool, List<Role>>> TryGetRolesAsyncByIDs(
            List<Guid> roleIDs, IRepository<Role, RoleRestricted> roleRepository)
        {
            List<Role> roles = null;

            if (roleIDs != null && roleIDs.Count > 0)
            {
                roles = await roleRepository.GetAllOrNothingByIDsAsync(roleIDs);

                if (roles == null)
                {
                    return Tuple.Create(false, roles);
                }
            }

            return Tuple.Create(true, roles);
        }
    }
}
